#!/bin/python3

# Copyright (c) 2020 Tim Roberts
# Author: Tim Roberts
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import enum
import os

# Creates directory if it doesn't already exist
def mkdirs(path):
    if not os.path.exists(path):
        os.makedirs(path)

# Lets the user pick from a list of options
# Returns the chosen option in lowercase
def choose(msg, options, default=""):
    if default != "":
        print(f"{msg} (default={default.title()}): ")
    else:
        print(msg)

    for i in range(len(options)):
        print("    " + str(i + 1) + ". ", options[i].title())

    choice = input("> ").lower()
    if choice == "":
        return default

    if choice.isnumeric():
        return options[int(choice) - 1].lower()

    for i in range(len(options)):
        option = options[i].lower()
        if option.find(choice) != -1:
            return option

    return default

def yesno(msg, default=False):
    if default == True:
        prompt = f"{msg} [Y/n]: "
    else:
        prompt = f"{msg} [y/N]: "

    str = input(prompt).lower()

    if str == "":
        return default

    if str.startswith("y"):
        return True
    else:
        return False

# Returns string for the target
def make_target(projectpath, projectname):
    choice = choose("Select target kind", ["executable", "static library", "shared library", "empty"], "executable")
    result = ""

    if choice == "empty":
        return ""

    name = input("target name (default=${PROJECT_NAME}): ")

    if name == "":
        name = "${PROJECT_NAME}"

    mkdirs(os.path.join(projectpath, "src"))
    mkdirs(os.path.join(projectpath, "include"))

    result = ""

    if choice == "executable":
        result += f"""# Add executable
add_executable({name} 
    src/main.c
)

target_include_directories({name} PRIVATE include)

"""
        entrypoint_data = """#include <stdio.h>

int main() {
    printf("Hello, World!\\n");
    return 0;
}

"""
        mainfile = os.path.join(projectpath, "src", "main.c")
        if not os.path.exists(mainfile):
            write_file(mainfile, entrypoint_data)

    elif choice == "static library":
        result += f"""# Add library

add_library({name} STATIC 
)

target_include_directories({name} PUBLIC include)

"""
    elif choice == "shared library":
        result += f"""# Add library
add_library({name} STATIC 
)

target_include_directories({name} PUBLIC include)

"""

    c_standard = choose("select C standard", ["C90", "C99", "C11"], "C99")
    
    result += f"# C standard\nset_target_properties({name} PROPERTIES C_STANDARD {c_standard[1::]})\n\n"

    # Add flags
    result += f"""# Compiler warnings
if (MSVC)
    target_compile_options({name}
        PRIVATE
        /W4
    )
else()
    target_compile_options({name} PRIVATE
        -Wall
        -Wextra
        -pedantic
        -Wshadow
        -Wpointer-arith
    )
endif()

"""

    return result

# Write CMakeLists.txt
def write_file(filename, data):
    if os.path.exists(filename)\
        and not yesno(f"Overwrite {filename}"):
            return

    file = open(filename, "w")
    file.write(data)
    file.close()

# Returns the path to parent CMakeList or empty string
def get_cmakeparent(projectpath, path, depth=5):
    if depth == 0:
        return ""

    parent = os.path.dirname(os.path.abspath(path))
    parentlist = os.path.join(parent, "CMakeLists.txt")
    if os.path.exists(parentlist):
        return parentlist
    else:
        return get_cmakeparent(projectpath, parent, depth - 1)

# Adds current project to subdirectory of parent CMakeList
def add_as_subdirectory(projectpath):
    parent = get_cmakeparent(projectpath, projectpath)
    if parent == "":
        return

    if not yesno(f"Add project to parent {parent}"):
        return

    file = open(parent, "a+")

    file.write(f"\nadd_subdirectory({os.path.basename(projectpath)})")
    file.close()

if len(sys.argv) > 1:
    projectpath = sys.argv[1]
else:
    projectpath = input("Project name: ")

if projectpath == "":
    print("Please provide a project name")
    exit()

if projectpath == ".":
    print("Initializing in current directory")
    projectname = os.path.basename(os.path.abspath("."))
else:
    projectname = os.path.basename(projectpath)


print("Creating project: ", projectname)

mkdirs(projectpath)

cmake_data = ""
cmake_data += f"""cmake_minimum_required(VERSION 3.1)
project({projectname})

"""

cmake_data += """# Check if cmake was run as toplevel or as subdirectory
set(CMAKE_TOPLEVEL FALSE)
if ("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")
    set(CMAKE_TOPLEVEL TRUE)
endif()

"""

# Only active if toplevel
cmake_data += """# Set top level settings
if(CMAKE_TOPLEVEL)
    # Set output directory
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

    # Build type.
    if(NOT CMAKE_BUILD_TYPE)
        message(STATUS "CMAKE_BUILD_TYPE not specified, default is 'Debug'")
        set(CMAKE_BUILD_TYPE "Debug" CACHE STRING "Choose the type of build" FORCE)
    else()
        message(STATUS "CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}")
    endif()
endif()

"""

cmake_data += """# Set debug option
if(CMAKE_BUILD_TYPE MATCHES Debug)
    set(DEBUG 1)
else()
    set(DEBUG 0)
endif()

"""

cmake_data += make_target(projectpath, projectname)

# Write CMakeLists.txt
filename = os.path.join(projectpath, "CMakeLists.txt")
write_file(filename, cmake_data)

add_as_subdirectory(projectpath)

# Add .gitignore
gitignore_data = """CMakeLists.txt.user
CMakeCache.txt
CMakeFiles
CMakeScripts
Testing
Makefile
cmake_install.cmake
install_manifest.txt
compile_commands.json
CTestTestfile.cmake
_deps
bin
lib
"""

if yesno("Add gitignore", True):
    filename = os.path.join(projectpath, ".gitignore")
    write_file(filename, gitignore_data)

