# cmake_init

cmake_init is a small python tools that initializes a c project using CMake

## Installation
```
git clone https://gitlab.com/ten3roberts/cmake_init.git
./cmake_init/install.sh
```

## Usage
Make sure cmake_init.py is in your path and marked as executable

```
cmake_init.py myproject
```

After answering the questions a folder named myproject will be created with a
CMakeLists.txt and .gitignore
